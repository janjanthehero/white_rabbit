<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit3.php");

use \PHPUnit\Framework\TestCase;
use WhiteRabbit3;

class WhiteRabbit3Test extends \PHPUnit\Framework\TestCase;
{
    /** @var WhiteRabbit3 */
    private $whiteRabbit3;

    public function setUp()
    {
        parent::setUp();
        $this->whiteRabbit3 = new WhiteRabbit3();

    }

    //SECTION FILE !
    /**
     * @dataProvider multiplyProvider
     */
    public function testMultiply($expected, $amount, $multiplier){
        $this->assertEquals($expected, $this->whiteRabbit3->multiplyBy($amount, $multiplier));
    }

    public function multiplyProvider(){
        return array(
            array(4, 2, 2),
            array(6, 3, 2),
            array(0.982, 0.491, 2),
            array(2.85, 0.57, 5),
            array(-2.001, -0.23, 8.7)
        );
    }
}
