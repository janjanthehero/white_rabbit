<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){

        // Declare variables
        $originalAmount = $amount;
        $coins = array(1,2,5,10,20,50,100);
        $coinAmount = array(0,0,0,0,0,0,0);
        $i = 0;

        rsort($coins); // Sort coins in descending order

        /* Calculates number of coins needed
        for the amount passed through the function */
        while($amount > 0)
        {
            $k = floor($amount / $coins[$i]); // Calculates number needed for given coin
            $coinAmount[$i] = $k; // Updates coin array with new value
            $amount = $amount - ($k * $coins[$i]); // Calculates the leftover amount

            $i++; //counter
        }

        $solution = array_combine($coins, $coinAmount); //Combine arrays

        //Print out the solution
        // echo("The minimum number of coins necessary for the amount " . $originalAmount . " are shown in the array below:<br>");

        // print_r($solution);
        
        return $solution;
    }
}

// Instantiate
// $myClass = new WhiteRabbit2;
// $myClass -> findCashPayment(792);

?>