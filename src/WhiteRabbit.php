<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        print_r(array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences));
    }

    /**
     * Parse the input file for letters.
     * parameter $filePath
     */
    private function parseFile ($filePath)
    {
        $path = $filePath;

        /*Read from textfile*/
        $parsedFile = (file_get_contents($path, NULL, NULL, 0, 5000));

        // return an all lowercase textfile
        return strtolower($parsedFile);
    }

    /**
     * Return the letter whose occurrences are the median.
     * parameter $parsedFile
     * parameter $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        

        // Get information about occurrences of characters in the string
        $strArray = count_chars($parsedFile, 1);

        // Remove duplicate values from array
        $strArray = array_unique($strArray);
        
        // Sort the array
        arsort($strArray);

        // Get all the value keys
        $keys = array_keys($strArray);

        // Find median character
        $medianCharacter = chr($keys[floor(count($keys) / 2)]);

        // Find how many times characted has occured
        $occurrences = substr_count($parsedFile, $medianCharacter);

        // Return values
        return $medianCharacter;
        return $occurrences;
    }
}

// instantiate
$myClass = new WhiteRabbit();
$myClass->findMedianLetterInFile("text1.txt");

?>